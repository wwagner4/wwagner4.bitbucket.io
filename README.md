# Entelijan Homepage

## run using docker
### clone or pull from git
```
# clone if not yet done
git clone --recurse-submdules https://wwagner4@bitbucket.org/wwagner4/wwagner4.bitbucket.io.git
cd wwagner4.bitbucket.io/
```
```
# Update if already cloned
# Change to the homepage repository /app/wwagner4.bitbucket.io/
git pull --recurse-submodules 
``` 
### build and run

```
./build.sh
nohup docker-compose up --no-color &
```
